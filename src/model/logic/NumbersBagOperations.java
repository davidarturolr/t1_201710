package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations <T extends Number>{
	public double computeMean(NumbersBag<T> bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<T> iter = (Iterator<T>) bag.getIterator();
			while(iter.hasNext()){
				mean += (Integer)iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(NumbersBag<T> bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<T> iter = ((NumbersBag<T>) bag).getIterator();
			while(iter.hasNext()){
				value = (Integer) iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}

	public int getPar(NumbersBag<T> bag)
	{
		int contador = 0;
		int value;

		if(bag != null){
			Iterator<T> iter = (Iterator<T>) bag.getIterator();
			while(iter.hasNext()){
				value = (Integer)iter.next();
				if( value %2 == 0){
					contador++;
				}
			}



		}
		return contador;
	}

	public double getPromedio(NumbersBag<T> bag) {
		double contador = 0;
		double cantidad = 0;
		int value;

		Iterator<T> iter = (Iterator<T>) bag.getIterator();
		while(iter.hasNext()){
			value = (Integer)iter.next();
			contador += value;
			cantidad++;


		}
		return contador/cantidad;
	}

	public double getNumberDivided (NumbersBag<T> bag)
	{

		double numero = computeMean( bag)/ getMax(bag);

		return numero;

	}

}
