package model.data_structures;
import java.util.ArrayList; 
import java.util.HashSet;
import java.util.Iterator;

public class NumbersBag <T extends Number>{
private HashSet<Integer> bag;
	
	public NumbersBag(){
		this.bag = (HashSet<Integer>) new HashSet<T>();
	}
	
	public NumbersBag(ArrayList<T> data){
		this();
		if(data != null){
			for (T datum : data) {
				bag.add((Integer) datum);
			}
		}
		
	}
	
	
	public void addDatum(Integer datum){
		bag.add(datum);
	}
	
	public Iterator<T> getIterator(){
		return (Iterator<T>) this.bag.iterator();
	}

	


}
