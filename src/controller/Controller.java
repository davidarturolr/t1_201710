package controller;

import java.util.ArrayList; 

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;


public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations ();
	
	
	public static NumbersBag<Integer> createBag(ArrayList<Integer> values){
         return new NumbersBag<Integer>(values);		
	}
	
	
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static int getPar(NumbersBag bag){
		return model.getPar(bag);
	}
	
	public static double getPromedio(NumbersBag bag){
		return model.getPromedio(bag);
	}
	
	public static double getNumberDivided(NumbersBag bag){
		return model.getNumberDivided(bag);
	}
	
}
