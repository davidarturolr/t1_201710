package view;

import java.util.ArrayList; 
import java.util.Scanner;

import controller.Controller;
import model.data_structures.NumbersBag;

public class MVCExample {
	
	
	
	
	private static void printMenu(){
		System.out.println("1. Create a new bag of integers (i.e., a set of unique integers)");
		System.out.println("2. Compute the mean");
		System.out.println("3. Get the max value");
		System.out.println("4. Get the number of pars");
		System.out.println("5. Get the average number");
		System.out.println("6. Get the mean value divided by the max value in the bag");
		System.out.println("7. Exit");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
	
	private static ArrayList<Integer> readData(){
		ArrayList<Integer> data = new ArrayList<Integer>();
		
		System.out.println("Please type the numbers (separated by spaces), then press enter: ");
		try{
			
			String line = new Scanner(System.in).nextLine();
		
			String[] values = line.split("\\s+");
			for(String value: values){
				data.add(new Integer(value.trim()));
			}
		}catch(Exception ex){
			System.out.println("Please type integer numbers separated by spaces");
		}
		return data;
	}

	public static void main(String[] args){
		
		NumbersBag bag = null;
		Scanner sc = new Scanner(System.in);
		
		for(;;){
		  printMenu();
		  
		 
		  int option = sc.nextInt();
		  switch(option){
			  case 1: bag = Controller.createBag(readData()); System.out.println("--------- \n The bag was created  \n---------");
			  break;
			  
			  case 2: System.out.println("--------- \n The mean value is "+Controller.getMean(bag)+" \n---------");
			  break;
			  
			  case 3: System.out.println("--------- \nThe max value is "+Controller.getMax(bag)+" \n---------");		  
			  break;
			  
			  case 4: System.out.println("--------- \n The number of pares is "+Controller.getPar(bag)+" \n---------");
			  break;
			  
			  case 5:  System.out.println("--------- \n The average number is "+Controller.getPromedio(bag)+" \n---------");
			  break;
			  
			  case 6:  System.out.println("--------- \n The mean value divided by the max value is "+Controller.getNumberDivided(bag)+" \n---------");
			  break;
			  
			  case 7: System.out.println("Bye!!  \n---------"); sc.close(); return;		  
			 
			  
			  default: System.out.println("--------- \n Invalid option !! \n---------");
		  }
		}
	}
	
}
